import './assets/loginstyle.css';
import BannerComponent from './banner.js';
import FormLogComponent from './formlog.js';

function Login() {
    return (
        <div className='loginpage'>
            <BannerComponent />
            <FormLogComponent />
        </div>
    );
}

export default Login;
