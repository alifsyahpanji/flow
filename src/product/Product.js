import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import LoadProduct from './LoadProduct';
import NavBottom from '../components/navbarbottom/NavBottom';
import ExtraNav from '../components/navbarbottom/ExtraNav';
import './assets/productstyle.css';
import buttonBackPng from './assets/iconarrow.png';
import buttonSearch from './assets/iconsearch.png';
import buttonFilter from './assets/iconfilter.png';




function Product() {
    const [product, setProduct] = useState(null);
    const navigate = useNavigate();
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const nameProduct = urlParams.get('name');
    const paramsPath = "/detail?id=";



    useEffect(() => {
        setTimeout(async () => {
            const res = await fetch("https://dummyjson.com/products/category/" + nameProduct);
            const data = await res.json();
            setProduct(data.products);
        }, 3000)
    })

    return (
        <div className="product d-flex justify-content-center">
            <div className="containercustom">

                <div className='addcart'>

                    <div className='my-1'>
                        <span onClick={() => navigate(-1)} className='mx-2 cursorpointer'><img src={buttonBackPng} alt="button back" /></span>
                        <span className='titlesinglecart'>Pencarian</span>
                    </div>

                    <div className='garisbesar my-3'></div>

                    <div className='containerpencarian d-flex justify-content-center'>

                        <div className='containertextfield d-flex align-items-center ms-3 me-auto'>
                            <div className='ms-2'><img src={buttonSearch} alt="button search" /></div>
                            <input className='inputcatatan form-control mx-2' type="text" placeholder='Mau cari apa' />

                        </div>

                        <div className='me-2'><img src={buttonFilter} alt="button filter" /></div>

                    </div>





                    {product && <div className='d-flex flex-wrap mx-3'>
                        {product.map(itemProduct => (
                            <div key={itemProduct.id} className='containeraddproduk mx-1 my-1'>
                                <div><img className='imgaddproduct' src={itemProduct.thumbnail} alt={itemProduct.title} /></div>

                                <div className='d-flex flex-column my-2'>
                                    <div className='titleaddproduk mx-1'>{itemProduct.title}</div>
                                    <div className='priceaddproduk mx-1 my-2'>${itemProduct.price}</div>
                                </div>

                                <div className='d-flex justify-content-center'><a href={paramsPath + itemProduct.id}>
                                    <button className='buttonaddcart'>View</button></a>
                                </div>
                            </div>
                        ))}
                    </div>
                    }


                    {!product && <div>
                        <LoadProduct />


                    </div>
                    }




                    <ExtraNav />
                    <NavBottom />
                </div>


            </div>
        </div>
    );
}

export default Product;