import './assets/profilstyle.css';
import ProfilHeader from './profilheader.js';
import ProfilMenu from './profilmenu.js';
import { Navigate } from "react-router-dom";
import NavBottom from '../components/navbarbottom/NavBottom';
import ExtraNav from '../components/navbarbottom/ExtraNav';



function Profil() {

    const checkUser = localStorage.getItem("usernameLog");

    if (checkUser) {
        return (
            <div className='profilpage'>
                <ProfilHeader />
                <ProfilMenu />

                <div className='d-flex justify-content-center'>
                    <ExtraNav />
                    <NavBottom />
                </div>

            </div>
        );
    } else {
        return <Navigate to="/" />;
    }


}

export default Profil;