import './assets/detailstyle.css';
import NavBottom from '../components/navbarbottom/NavBottom';
import ExtraNav from '../components/navbarbottom/ExtraNav';
import React, { useState, useEffect } from 'react';
import LoadDetail from './LoadDetail';

const handleClick = event => {

    localStorage.setItem("iditem", event.currentTarget.id);
    window.location.replace('/mycart');
};

function Detail() {
    const [detail, setDetail] = useState(null);
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const idDetail = urlParams.get('id');


    useEffect(() => {
        setTimeout(async () => {
            const res = await fetch("https://dummyjson.com/products/" + idDetail);
            const data = await res.json();
            setDetail(data);
        }, 3000)
    })


    return (

        <div className='detailpage d-flex justify-content-center'>
            <div className="containercustom">


                {detail &&
                    <div>
                        <div className='my-2 mx-2'><img src={detail.thumbnail} className='imagedetail' alt={detail.title} /></div>

                        <div className='my-1 mx-2'><span className='categorytag'>{detail.category}</span></div>

                        <div className='titledetail my-3 mx-2'>{detail.title}</div>

                        <div className='pricedetail mx-2'>${detail.price}</div>

                        <div className='garis my-3 mx-2'></div>

                        <div className='titledeskripsi mx-2'>Deskripsi Produk</div>

                        <div className='deskripsi my-1 mx-2'>{detail.description}</div>

                        <button id={detail.id} className='btndetail my-2 mx-2 text-center' onClick={handleClick}>Tambah Pesanan</button>
                    </div>

                }

                {!detail &&
                    <div>
                        <LoadDetail />
                    </div>
                }



                <ExtraNav />
                <NavBottom />
            </div>
        </div>
    );
}

export default Detail;