import './assets/detailstyle.css';

function LoadDetail() {
    return (
        <div>

            <div className='loaderimg my-2 mx-2'>
                <div className="shimmer-wrapper">
                    <div className="shimmer"></div>
                </div>
            </div>

            <div className='loadtitle my-3 mx-2'>
                <div className="shimmer-wrapper">
                    <div className="shimmer"></div>
                </div>
            </div>

            <div className='garis my-3 mx-2'></div>

            <div className='loaddeskripsi my-2 mx-2'>
                <div className="shimmer-wrapper">
                    <div className="shimmer"></div>
                </div>
            </div>

        </div>
    );
}

export default LoadDetail;