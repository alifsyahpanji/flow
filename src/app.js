import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './login/login';
import Category from './category/Category';
import Product from './product/Product';
import Detail from './detail/Detail';
import MyCart from './mycart/MyCart';
import Profil from './profil/Profil';

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Login />} />
                <Route path="category" element={<Category />} />
                <Route path="product" element={<Product />} />
                <Route path="detail" element={<Detail />} />
                <Route path="mycart" element={<MyCart />} />
                <Route path="profil" element={<Profil />} />

            </Routes>
        </BrowserRouter>
    );
}

export default App;
