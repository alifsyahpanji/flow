import React, { useState, useEffect } from 'react';
import iconCategory from './assets/iconcategory.png';
import LoadCategory from './LoadCategory';

function CategoryData() {
    const [category, setCategory] = useState(null);
    const paramsPath = "/product?name=";


    useEffect(() => {
        setTimeout(async () => {
            const res = await fetch("https://dummyjson.com/products/categories");
            const data = await res.json();
            setCategory(data);
        }, 3000)
    })

    return (
        <div>

            {category && <div className='categoryallitem d-flex flex-wrap justify-content-center'>

                {category.map(itemCategory => (
                    <div key={itemCategory} className='categoryitem mx-1 my-2'>
                        <a className='categorylink' href={paramsPath + itemCategory}>
                            <div><img src={iconCategory} alt="icon category" /></div>
                            <div className='categorytext text-center'>{itemCategory}</div>
                        </a>
                    </div>
                ))}
            </div>

            }

            {!category && <div>
                <LoadCategory />
            </div>
            }

        </div>
    );

}
export default CategoryData;